# Options Hashes
#
#Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```



def transmogrify(string, options = {})

  defaults = {
    times: 1,
    upcase: nil,
    reverse: nil
  }

  options = defaults.merge(options)
  str = string
  t = options[:times]

  str = string.upcase if options[:upcase]
  str = string.reverse if options[:reverse]
  str = string.upcase.reverse if options[:upcase] && options[:reverse]

  str * t
end
